db.users.insertMany([
    {
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
       firstName: "Jane",
        lastName: "Doe",
        age: 21,
        contact: {
            phone: "87654321",
            email: "janedoe@gmail.com"
        },
        courses: ["CSS", "JavaScript", "Python"],
        department: "HR"
    },
]);

// Find users with with s or d in the last name




db.collectionName.find({ $or: [{ fieldA: valueA}, {fieldB: valueB} ] });


db.users.find({ $or: [{ firstName: {$regex: 'S', $options: '$i'}}, {lastName: {$regex: 'D', $options: '$i'}} ] })

db.users.find(
    { firstName: "Jane"},
    {
        firstName: 1,
        lastName: 1,
        _id: 0
    },
    { firstName: "Stephen"},
    {
        firstName: 1,
        lastName: 1,
        _id: 0
    }
);


// greater than and age


db.users.find({ $and: [{ department: "HR" }, {age: {$gte: 70}} ] });


// users letter e first name and age less than or equal to 30

db.users.find({ $and: [{ firstName: {$regex: 'E', $options: '$i'} }, {age: {$lte: 30}} ] });

